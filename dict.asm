%include "lib.inc"
%ifndef SIZE
%define SIZE 8
%endif

section .text

global find_word

find_word:
	.loop:  test rsi, rsi
		jz .not_exists
		add rsi, SIZE
		push rdi
		push rsi
		call string_equals
		pop rsi
		pop rdi
		sub rsi, SIZE
		cmp rax, 1
		je .exists
		mov rsi, [rsi]
		jmp .loop
	.exists:
		mov rax, rsi
		ret
	.not_exists: 	
		mov rax, 0
		ret
		 
	
	
