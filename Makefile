ASM        = nasm
ASMFLAGS   =-felf64 -o

start: lib.o dict.o main.o
	ld -o start main.o lib.o dict.o

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) lib.o lib.asm

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) dict.o dict.asm

main.o: main.asm dict.o lib.o words.inc colon.inc
	$(ASM) $(ASMFLAGS) main.o main.asm