%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define RETURN_GOOD 0
%define RETURN_ERROR 1
%define BUF_SIZE 256
%define STDERR 2
%define SIZE 8

section .rodata

success_message: db "Word exists in the dictionary", 0
fail_message: db "Word does not exist in the dictionary", 0
error_message: db "Input mistake", 0

section .text
global _start

_start:
	sub rsp, BUF_SIZE
	mov rdi, rsp
	mov rsi, BUF_SIZE
	call read_word
	test rdx, rdx
	jz .error
	
	mov rdi, rax
	mov rsi, NEXT_ENTRY
	call find_word
	test rax, rax
	jz .fail
	
.success:
	add rax, SIZE
	add rsp, BUF_SIZE
	push rax
	mov rdi, success_message
	call print_string
	pop rdi
	push rdi
	call string_length
	pop rdi
	inc rax
	add rdi, rax
	call print_string
	mov rdi, RETURN_GOOD
	call exit
	
.fail:
	mov rdi, fail_message
	mov rsi, STDERR
	call print_string
	mov rdi, RETURN_GOOD
	call exit
.error:
	mov rdi, error_message
	mov rsi, STDERR
	call print_string
	mov rdi, RETURN_ERROR
	call exit

