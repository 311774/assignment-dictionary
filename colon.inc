%ifndef NEXT_ENTRY
%define NEXT_ENTRY 0
%endif
%macro colon 2
	%%NEXT_ENTRY: dq NEXT_ENTRY db %1, 0
	%2: %define NEXT_ENTRY %%NEXT_ENTRY
%endmacro

